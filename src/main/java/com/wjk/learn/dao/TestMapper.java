package com.wjk.learn.dao;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * TODO
 *
 * @author weijunkang
 * @version 1.0
 * @date 2021/2/6 17:46
 */
//public interface TestMapper extends BaseMapper{
public interface TestMapper{

    /**
     * 查询全部
     *
     * @return
     */
    @Select("select * from seller_info")
    List<JSONObject> list();

}
