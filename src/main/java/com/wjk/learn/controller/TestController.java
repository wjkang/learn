package com.wjk.learn.controller;

import cn.hutool.json.JSONObject;
import com.wjk.learn.dao.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO
 *
 * @author weijunkang
 * @version 1.0
 * @date 2021/2/6 17:43
 */
@RestController
public class TestController {

    @Autowired
    private TestMapper testMapper;

    @GetMapping("/")
    public List<JSONObject> test() {
        return testMapper.list();
    }

}
